
function sleep(ms) {
    return new Promise(
      resolve => setTimeout(resolve, ms)
    );
}  

async function toggle(str){
    var element = document.getElementsByClassName('box');

    if( str == "open" ){
        for( let i = 0 ; i < element.length ; i+=1 ){
            element[i].style.display = "block";
            await sleep(100*i+100);
        }
    }
    else if( str == "close" ){
        for( let i = element.length - 1 ; i >= 0 ; i-=1 ){
            element[i].style.display = "none";
            await sleep(100*i+100);
        }
    }        
}

function FetchingImg(imag){
  fetch(imag).then(res =>
        res.blob()).then(blob => {
            document.getElementById('image').style.backgroundImage = "url("+imag+")";
    })
}